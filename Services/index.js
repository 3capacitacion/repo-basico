//Importamos los controladores
import { PersonalData, PreferencesColor } from "../Controllers";

export function Servicio({ info, color }) {
  const personalData = PersonalData({ info });
  const preferencesColor = PreferencesColor({ color });
  return { personalData, preferencesColor };
}
