//Importamos el adaptador
import {Adaptador} from "./Adapters";

const result = Adaptador({
  color: "negro", 
  info: { //ingresamos lo datos persona-color
    nombre: "Angelo",
    edad: 25,
    nacionalidad: "Peru",
    domicilio: "Lima",
    editorTexto: "Editor Atom", 
    telefono: 12345,
    signo: "cancer",
  },
});
console.log(result);

